﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManagementSystem
{
    public partial class Employee
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }
        public List<Detail> Details { get; set; }
    }
}
