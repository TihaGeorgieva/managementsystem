﻿namespace ManagementSystem
{
    public partial class Detail
    {
        public int Id { get; set; }

        public int TypeId { get; set; }

        public DetailType DetailType { get; set; }

        public string Value { get; set; }

        public int EmployeeId { get; set; }
    }
}
