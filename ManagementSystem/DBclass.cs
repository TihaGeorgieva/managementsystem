﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace ManagementSystem
{
    public class DBclass
    {
        private readonly string connectString = "server=localhost;user id=root;database=managment_system; pwd=root";
        private MySqlConnection connection;

        public DBclass()
        {
            this.connection = new MySqlConnection(connectString);
            this.connection.Open();
        }

        ~DBclass()
        {
            if (this.connection != null)
            {
                this.connection.Close();
                this.connection = null;
            }
        }

        public List<Employee> GetAllEmployees()
        {
            List<Employee> employees = new List<Employee>();
            try
            {
                string sql = "SELECT * FROM employees";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                using (var reader = mySqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Employee employee = new Employee();

                        employee.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        employee.LastName = reader.GetString(reader.GetOrdinal("LastName"));
                        employee.Name = reader.GetString(reader.GetOrdinal("Name"));

                        employees.Add(employee);
                    }
                }

                foreach (var employee in employees)
                {
                    employee.Details = this.GetAllDetailsByEmployeeId(employee.Id);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return employees;
        }

        public List<DetailType> GetAllDetailTypes()
        {
            List<DetailType> detail_Types = new List<DetailType>();
            try
            {
                string sql = "SELECT * FROM details_type";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                using (var reader = mySqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        DetailType detail_Type = new DetailType();
                        detail_Type.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        detail_Type.TypeName = reader.GetString(reader.GetOrdinal("TypeName"));

                        detail_Types.Add(detail_Type);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return detail_Types;
        }

        public bool InsertDetailInEmployee(Detail detail)
        {
            bool isSuccess = false;
            try
            {
                string sql = "INSERT details SET TypeId = @TypeId, Value=@Value, EmployeeId= @EmployeeID";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                mySqlCommand.Parameters.AddWithValue("@TypeId", detail.TypeId);
                mySqlCommand.Parameters.AddWithValue("@Value", detail.Value);
                mySqlCommand.Parameters.AddWithValue("@EmployeeID", detail.EmployeeId);

                int rows = mySqlCommand.ExecuteNonQuery();
                if (rows > 0)
                {
                    isSuccess = true;
                    //DeleteDetails(detail);
                }
                else
                {
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return isSuccess;
        }
        public List<Detail> GetAllDetailsByEmployeeId(int employeeID)
        {
            List<Detail> details = new List<Detail>();
            try
            {
                string sql = "SELECT * FROM details where EmployeeId = @EmployeeID";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                mySqlCommand.Parameters.AddWithValue("EmployeeID", employeeID);

                using (var reader = mySqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Detail detail = new Detail();

                        detail.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        detail.TypeId = reader.GetInt32(reader.GetOrdinal("TypeId"));
                        detail.Value = reader.GetString(reader.GetOrdinal("Value"));
                        detail.EmployeeId = reader.GetInt32(reader.GetOrdinal("EmployeeId"));

                        details.Add(detail);
                    };
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return details;
        }

        public List<Detail> GetAllEmployeeDetails(int employeeID)
        {
            List<Detail> details = new List<Detail>();
            try
            {
                string sql = "SELECT d.ID, TypeName, Value, TypeID FROM details as d" +
                             " inner join details_type on details_type.Id = d.TypeId" +
                             " where d.EmployeeId = " + employeeID;

                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                using (var reader = mySqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Detail detail = new Detail();

                        detail.EmployeeId = employeeID;
                        detail.DetailType = new DetailType();
                        detail.DetailType.Id = reader.GetInt32(reader.GetOrdinal("TypeID"));
                        detail.DetailType.TypeName = reader.GetString(reader.GetOrdinal("TypeName"));
                        detail.Id = reader.GetInt32(reader.GetOrdinal("ID"));
                        detail.TypeId = detail.DetailType.Id;
                        detail.Value = reader.GetString(reader.GetOrdinal("Value"));

                        details.Add(detail);
                    }
                }

                return details;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }
        public List<Detail> GetAllDetails()
        {
            List<Detail> details = new List<Detail>();
            try
            {
                string sql = "SELECT  det.TypeId, det.Value, det.EmployeeId FROM details det";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);
                using (var reader = mySqlCommand.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Detail detail = new Detail();
                        detail.Id = reader.GetInt32(reader.GetOrdinal("Id"));
                        detail.TypeId = reader.GetInt32(reader.GetOrdinal("TypeId"));
                        detail.Value = reader.GetString(reader.GetOrdinal("Value"));
                        detail.EmployeeId = reader.GetInt32(reader.GetOrdinal("EmployeeId"));

                        details.Add(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return details;
        }
        public int InsertEmployee(Employee employee)
        {
            try
            {
                string sql = "INSERT INTO employees (Name, LastName) VALUES (@Name, @LastName)";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                mySqlCommand.Parameters.AddWithValue("Name", employee.Name);
                mySqlCommand.Parameters.AddWithValue("LastName", employee.LastName);

                int rows = mySqlCommand.ExecuteNonQuery();
                int insertedEmployeeID = 0;
                if (rows > 0)
                {
                    sql = "SELECT LAST_INSERT_ID() as ID";
                    mySqlCommand = new MySqlCommand(sql, this.connection);
                    using (var reader = mySqlCommand.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            insertedEmployeeID = reader.GetInt32(reader.GetOrdinal("ID"));
                        }
                    }
                }

                foreach (var detail in employee.Details)
                {
                    detail.EmployeeId = insertedEmployeeID;
                    this.InsertDetailInEmployee(detail);
                }

                return insertedEmployeeID;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return -1;
            }
        }

        //public bool InsertDetails(Detail details)
        //{
        //    bool isSuccess = false;
        //    try
        //    {
        //        string sql = "INSERT INTO details (TypeId, Value, EmployeeId) VALUES (@TypeId, @Value, @EmployeeID)";

        //        MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);
        //        mySqlCommand.Parameters.AddWithValue("@TypeId", details.TypeId);
        //        mySqlCommand.Parameters.AddWithValue("@Value", details.Value);
        //        mySqlCommand.Parameters.AddWithValue("@EmployeeID", details.EmployeeId);

        //        int rows = mySqlCommand.ExecuteNonQuery();
        //        if (rows > 0)
        //        {
        //            isSuccess = true;
        //            // DeleteDetails(details);
        //        }
        //        else
        //        {
        //            isSuccess = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Console.WriteLine(ex);
        //    }

        //    return isSuccess;
        //}

        public bool UpdateEmployee(Employee employee)
        {
            bool isSuccess = false;

            try
            {
                string sql = "UPDATE employees SET Name=@Name, LastName=@LastName WHERE Id=@Id";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                mySqlCommand.Parameters.AddWithValue("@Id", employee.Id);
                mySqlCommand.Parameters.AddWithValue("@Name", employee.Name);
                mySqlCommand.Parameters.AddWithValue("@LastName", employee.LastName);

                int rows = mySqlCommand.ExecuteNonQuery();
                if (rows > 0)
                {
                    isSuccess = true;
                    this.DeleteDetailsByEmployeeId(employee.Id);

                    var detail = employee.Details.FirstOrDefault();
                    if (detail != null)
                    {
                        this.InsertDetailInEmployee(detail);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return isSuccess;
        }

        public bool UpdateDetails(Detail details)
        {
            bool isSuccess = false;

            try
            {
                string sql = "UPDATE details SET TypeId=@TypeId, Value=@Value WHERE EmployeeId=@EmployeeID and Id = @Id";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                mySqlCommand.Parameters.AddWithValue("@Id", details.Id);
                mySqlCommand.Parameters.AddWithValue("@EmployeeID", details.EmployeeId);
                mySqlCommand.Parameters.AddWithValue("@TypeId", details.TypeId);
                mySqlCommand.Parameters.AddWithValue("@Value", details.Value);

                int rows = mySqlCommand.ExecuteNonQuery();
                if (rows > 0)
                {
                    isSuccess = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return isSuccess;
        }

        public bool DeleteEmployee(Employee employee)
        {
            bool isSuccess = false;
            try
            {
                DeleteDetailsByEmployeeId(employee.Id);
                string sql = "DELETE FROM employees WHERE Id=@Id";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                mySqlCommand.Parameters.AddWithValue("@Id", employee.Id); ;
                int rows = mySqlCommand.ExecuteNonQuery();

                if (rows > 0)
                {
                    isSuccess = true;
                }
                else
                {
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return isSuccess;
        }

        public bool DeleteDetails(Detail details)
        {
            bool isSuccess = false;

            try
            {
                string sql = "DELETE FROM details WHERE Id=@Id";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                mySqlCommand.Parameters.AddWithValue("@Id", details.Id);

                int rows = mySqlCommand.ExecuteNonQuery();
                if (rows > 0)
                {
                    isSuccess = true;

                }
                else
                {
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return isSuccess;
        }

        public bool DeleteDetailsByEmployeeId(int employeeID)
        {
            bool isSuccess = false;

            try
            {
                string sql = "DELETE FROM details WHERE EmployeeId=@EmployeeID";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                mySqlCommand.Parameters.AddWithValue("@EmployeeID", employeeID);

                int rows = mySqlCommand.ExecuteNonQuery();
                if (rows > 0)
                {
                    isSuccess = true;
                }
                else
                {
                    isSuccess = false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return isSuccess;
        }

        public List<Employee> SearchEmployeeByName(string name)
        {
            List<Employee> employees = null;

            try
            {
                string sql = "SELECT * FROM employees WHERE Name like '%" + name + "%' or LastName like '%" + name + "%'";
                MySqlCommand mySqlCommand = new MySqlCommand(sql, connection);

                mySqlCommand.CommandType = CommandType.Text;

                using (MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader())
                {
                    while (mySqlDataReader.Read())
                    {
                        if (employees == null)
                        {
                            employees = new List<Employee>();
                        }

                        employees.Add(new Employee()
                        {
                            Id = mySqlDataReader.GetInt32(mySqlDataReader.GetOrdinal("Id")),
                            Name = mySqlDataReader.GetString(mySqlDataReader.GetOrdinal("Name")),
                            LastName = mySqlDataReader.GetString(mySqlDataReader.GetOrdinal("LastName")),
                        });
                    }
                }

                if (employees != null)
                {
                    foreach (var employee in employees)
                    {
                        employee.Details = this.GetAllDetailsByEmployeeId(employee.Id);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return employees;
        }

        public bool NameRequired(string name)
        {
            bool isRight = false;

            if (name != "")
            {
                isRight = true;
            }
            else
            {
                isRight = false;
            }
            return isRight;
        }

        public bool OnlyNumbersInMobilePhone(string number)
        {
            bool isRight = false;

            if (number.Length >= 10 && number.Length <= 15 && number.All(char.IsDigit))
            {
                isRight = true;
            }
            else
            {
                isRight = false;
            }
            return isRight;
        }

        public bool ValidEmailAddress(string email)
        {
            bool isRight = false;
            string pattern = "^([0-9a-zA-Z]([-\\.\\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\\w]*[0-9a-zA-Z]\\.)+[a-zA-Z]{2,9})$";


            if (Regex.IsMatch(email, pattern))
            {
                isRight = true;
            }
            else
            {
                isRight = false;
            }
            return isRight;
        }
    }
}
