﻿namespace ManagementSystem
{
    public partial class DetailType
    {
        public int Id { get; set; }

        public string TypeName { get; set; }
    }
}
