﻿using System.Collections.Generic;
using System.Windows;

namespace ManagementSystem
{
    /// <summary>
    /// Interaction logic for AddDetail.xaml
    /// </summary>
    public partial class AddDetail : Window
    {
        private readonly DBclass DB;
        private List<DetailType> detailTypes;
        private List<Employee> employees;
        public AddDetail()
        {
            InitializeComponent();
            this.DB = new DBclass();
            Init();
        }

        private void Init()
        {
            this.LoadDetailsComboBox();
            this.LoadEmployeesComboBox();
        }

        private void LoadDetailsComboBox()
        {
            this.detailTypes = this.DB.GetAllDetailTypes();
            this.BoxForDetailType.ItemsSource = this.detailTypes;
        }

        private void LoadEmployeesComboBox()
        {
            this.employees = this.DB.GetAllEmployees();
            this.BoxForEmployees.ItemsSource = this.employees;
        }
        private void Save_Button(object sender, RoutedEventArgs e)
        {
            if (BoxForEmployees != null && BoxForDetailType != null && TextBoxForDetail != null)
            {
                var selectedDetailType = this.BoxForDetailType.SelectedItem as DetailType;
                var selectedEmployee = this.BoxForEmployees.SelectedItem as Employee;

                Detail newDetail = new Detail()
                {
                    EmployeeId = selectedEmployee.Id,
                    Value = TextBoxForDetail.Text,
                    TypeId = selectedDetailType.Id,
                };

                if (DB.OnlyNumbersInMobilePhone(newDetail.Value) == false && selectedDetailType.Id == 1)
                {
                    TextBlockForOnlyNumbers.Visibility = Visibility.Visible;
                }
                else if (DB.ValidEmailAddress(newDetail.Value) == false && selectedDetailType.Id == 3)
                {
                    TextBlockForEmail.Visibility = Visibility.Visible;
                }
                else
                {
                    TextBlockForOnlyNumbers.Visibility = Visibility.Hidden;
                    TextBlockForEmail.Visibility = Visibility.Hidden;

                    bool isSuccess = DB.InsertDetailInEmployee(newDetail);
                    if (isSuccess)
                    {
                        MessageBox.Show("Successful record!");
                    }
                    else
                    {
                        MessageBox.Show("Wrong input!");
                    }
                }
            }
        }
    }
}