﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ManagementSystem
{
    /// <summary>
    /// Interaction logic for Details.xaml
    /// </summary>
    public partial class DetailsPage : Window
    {
        private readonly DBclass DB;
        private List<DetailType> detailTypes;
        private int selectedEmployeeID = -1;

        public DetailsPage(int selectedEmployeeID)
        {
            InitializeComponent();
            this.DB = new DBclass();
            this.selectedEmployeeID = selectedEmployeeID;
            Init();
        }

        private void Init()
        {
            this.LoadDetailsComboBox();
            this.LoadDetails();
        }

        private void LoadDetailsComboBox()
        {
            this.detailTypes = this.DB.GetAllDetailTypes();
            this.BoxForDetailType.ItemsSource = this.detailTypes;
        }

        private void LoadDetails()
        {
            this.dataInfo.ItemsSource = this.DB.GetAllEmployeeDetails(this.selectedEmployeeID);
        }

        private void dataInfo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Detail selectedDetail = this.dataInfo.SelectedItem as Detail;

            if (selectedDetail != null)
            {
                foreach (var detailType in this.BoxForDetailType.ItemsSource)
                {
                    if ((detailType as DetailType).Id == selectedDetail.DetailType.Id)
                    {
                        this.BoxForDetailType.SelectedItem = detailType;
                        break;
                    }
                }

                this.TextBoxForDetail.Text = selectedDetail.Value;
            }
        }

        private void Save_Button(object sender, RoutedEventArgs e)
        {
            if (BoxForDetailType.Text != null)
            {
                var selectedDetailType = this.BoxForDetailType.SelectedItem as DetailType;
                Detail selectedDetail = this.dataInfo.SelectedItem as Detail;

                if (selectedDetailType != null)
                {
                    selectedDetail.DetailType = selectedDetailType;
                    selectedDetail.TypeId = selectedDetailType.Id;
                    selectedDetail.Value = this.TextBoxForDetail.Text;

                    if (DB.OnlyNumbersInMobilePhone(selectedDetail.Value) == false && selectedDetailType.Id == 1)
                    {
                        TextBlockForOnlyNumbers.Visibility = Visibility.Visible;
                    }
                    else if (DB.ValidEmailAddress(selectedDetail.Value) == false && selectedDetailType.Id == 3)
                    {
                        TextBlockForEmail.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        TextBlockForOnlyNumbers.Visibility = Visibility.Hidden;
                        TextBlockForEmail.Visibility = Visibility.Hidden;
                        if (selectedDetail.Id > 0)
                        {
                            bool isSuccess = DB.UpdateDetails(selectedDetail);
                            if (isSuccess)
                            {
                                MessageBox.Show("Successful record!");
                            }
                            else
                            {
                                MessageBox.Show("Wrong input!");
                            }

                            this.LoadDetails();
                        }
                    }
                }
            }
        }

        private void Delete_Button(object sender, RoutedEventArgs e)
        {
            Detail selectedDetail = this.dataInfo.SelectedItem as Detail;

            TextBlockForDetail.Visibility = Visibility.Hidden;

            if (selectedDetail != null)
            {
                this.DB.DeleteDetails(selectedDetail);
            }
            else
            {
                TextBlockForDetail.Visibility = Visibility.Visible;
            }
            this.LoadDetails();
        }
    }
}
