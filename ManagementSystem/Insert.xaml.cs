﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace ManagementSystem
{
    /// <summary>
    /// Interaction logic for Insert.xaml
    /// </summary>
    public partial class Insert : Window
    {
        private readonly DBclass DB;
        private List<DetailType> detailTypes;
        private List<Employee> employees;
        private int selectedEmployeeID = -1;

        public Insert()
        {
            this.InitializeComponent();
            this.DB = new DBclass();
            this.Init();
        }

        private void Init()
        {
            this.LoadDetailsComboBox();
            this.LoadEmployees();
        }

        private void LoadDetailsComboBox()
        {
            this.detailTypes = this.DB.GetAllDetailTypes();
            this.BoxForDetailType.ItemsSource = this.detailTypes;
        }

        private void LoadEmployees()
        {
            this.employees = this.DB.GetAllEmployees();
            this.dataInfo.ItemsSource = this.employees;
        }

        private void dataInfo_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Employee selectedEmployee = this.dataInfo.SelectedItem as Employee;

            if (selectedEmployee != null)
            {
                this.selectedEmployeeID = selectedEmployee.Id;
                this.TextBoxFirstName.Text = selectedEmployee.Name;
                this.TextBoxForLastName.Text = selectedEmployee.LastName;

                // var detailsType = this.detailTypes.FirstOrDefault(dt => dt.Id == selectedContact.Details.First().TypeId);
                Detail selectedDetail = selectedEmployee.Details.FirstOrDefault();

                if (selectedDetail != null)
                {
                    foreach (var dt in this.detailTypes)
                    {
                        if (dt.Id == selectedDetail.TypeId)
                        {
                            this.BoxForDetailType.SelectedItem = dt;
                            break;
                        }
                    }
                    this.TextBoxForDetail.Text = selectedDetail.Value;
                }
                else
                {
                    this.TextBoxForDetail.Text = string.Empty;
                    this.BoxForDetailType.SelectedItem = null;
                }
            }
        }
        private void Save_Button(object sender, RoutedEventArgs e)
        {
            Employee employeeForUpdate = new Employee()
            {
                Id = this.selectedEmployeeID,
                LastName = this.TextBoxForLastName.Text,
                Name = this.TextBoxFirstName.Text,
            };

            employeeForUpdate.Details = new List<Detail>();

            if (this.BoxForDetailType.SelectedItem != null)
            {
                var selectedDetailType = this.BoxForDetailType.SelectedItem as DetailType;

                if (selectedDetailType != null)
                {
                    employeeForUpdate.Details.Add(new Detail()
                    {
                        EmployeeId = this.selectedEmployeeID,
                        TypeId = selectedDetailType.Id,
                        Value = this.TextBoxForDetail.Text,
                    });

                    if (DB.OnlyNumbersInMobilePhone(TextBoxForDetail.Text) == false && selectedDetailType.Id == 1)
                    {
                        TextBlockForOnlyNumbers.Visibility = Visibility.Visible;
                    }
                    else if (DB.ValidEmailAddress(TextBoxForDetail.Text) == false && selectedDetailType.Id == 3)
                    {
                        TextBlockForEmail.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        TextBlockForOnlyNumbers.Visibility = Visibility.Hidden;
                        TextBlockForEmail.Visibility = Visibility.Hidden;
                    }
                }
            }

            if (DB.NameRequired(employeeForUpdate.Name) == false)
            {
                TextBlockForRequiredName.Visibility = Visibility.Visible;
            }
            else
            {
                TextBlockForRequiredName.Visibility = Visibility.Hidden;
                if (this.selectedEmployeeID > 0)
                {
                    bool isSuccess = DB.UpdateEmployee(employeeForUpdate);
                    this.LoadEmployees();
                    if (isSuccess)
                    {
                        MessageBox.Show("Successful record!");
                    }
                }
                else
                {
                    int newAddedID = DB.InsertEmployee(employeeForUpdate);
                    if (newAddedID > 0)
                    {
                        this.selectedEmployeeID = newAddedID;
                        this.LoadEmployees();
                        MessageBox.Show("Successful record!");
                    }
                }
            }

            this.dataInfo.SelectedItem = this.employees.FirstOrDefault(c => c.Id == this.selectedEmployeeID);
        }
        private void Add_Button(object sender, RoutedEventArgs e)
        {
            this.selectedEmployeeID = -1;
            this.TextBoxFirstName.Text = string.Empty;
            this.TextBoxForLastName.Text = string.Empty;
            this.BoxForDetailType.SelectedItem = null;
            this.TextBoxForDetail.Text = string.Empty;
        }
    }
}