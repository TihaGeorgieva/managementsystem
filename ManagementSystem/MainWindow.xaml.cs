﻿using System.Collections.Generic;
using System.Windows;

namespace ManagementSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Employee> employees;
        private readonly DBclass DB;
        public MainWindow()
        {
            InitializeComponent();
            this.DB = new DBclass();
            LoadEmployees();
        }
        private void LoadEmployees()
        {
            this.employees = this.DB.GetAllEmployees();
            this.dataInfo.ItemsSource = this.employees;
        }
        private void Add(object sender, RoutedEventArgs e)
        {
            Insert insert = new Insert();
            insert.Show();
            LoadEmployees();
        }
        private void Delete(object sender, RoutedEventArgs e)
        {
            Employee selectedEmployee = this.dataInfo.SelectedItem as Employee;

            TextBlockForDelete.Visibility = Visibility.Hidden;

            if (selectedEmployee != null)
            {
                this.DB.DeleteEmployee(selectedEmployee);
            }
            else
            {
                TextBlockForDelete.Visibility = Visibility.Visible;
            }

            this.LoadEmployees();
        }
        private void ShowDetails(object sender, RoutedEventArgs e)
        {
            TextBlockForDetails.Visibility = Visibility.Hidden;

            if (this.dataInfo.SelectedItem != null)
            {
                var selectedEmployee = this.dataInfo.SelectedItem as Employee;
                DetailsPage detailsPage = new DetailsPage(selectedEmployee.Id);
                detailsPage.Show();
            }
            else
            {
                TextBlockForDetails.Visibility = Visibility.Visible;
            }
        }
        private void Search_Button(object sender, RoutedEventArgs e)
        {
            dataInfo.ItemsSource = this.DB.SearchEmployeeByName(TextBoxForSearchName.Text);
        }

        private void Refresh(object sender, RoutedEventArgs e)
        {
            this.LoadEmployees();
            TextBoxForSearchName.Text = string.Empty;
        }

        private void AddDetailButton(object sender, RoutedEventArgs e)
        {
            AddDetail addDetail = new AddDetail();
            addDetail.Show();
        }
    }
}
